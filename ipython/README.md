# Apache Spark + iPython

1. Install virtualenv.

    Virtualenv is a useful tool that creates isolated Python development environments where you can do all your development work.

    It's possible that your system already has virtualenv. Refer to the command line, and try running:

    `virtualenv --version`

    If the command was not found, use easy_install or pip to install virtualenv.

    `easy_install virtualenv`

    or

    `pip install virtualenv`

2. Install iPython.

    After installing virtualenv, you can create a new isolated development environment, like so:

    `virtualenv notebook`

    Here, virtualenv creates a folder, notebook/, and sets up a clean copy of Python inside for you to use. It also installs the handy package manager, pip.

    Enter your newly created development environment and activate it so you can begin working within it.

    `cd notebook`
    `scripts\activate`

    Now, you can safely install ipython:

    `pip install ipython`

    Or if you want to also get the dependencies for the IPython notebook:

    `pip install "ipython[notebook]"`

3. Install py4j.

    Run `pip install py4j` or `easy_install py4j`

    ref:

        *https://www.py4j.org/install.html#install-instructions

4. Start your iPython notebook/ Using the Notebook.

    Now that IPython is installed, you will have to identify where you would like the notebooks you create to be saved. Navigate to that directory and launch the IPython Notebook. To launch the notebook, run the following:

    `ipython notebook`

5. Create a new notebook.

    If no notebook exist yet, click 'New' on the top right, then select, 'Python 2'.

6. Place your script in the input field then click the play button. You should see the result on the same page under your script.

## Ref:

    * https://www.safaribooksonline.com/blog/2013/12/12/start-ipython-notebook/
    * http://stackoverflow.com/questions/26315726/importerror-no-module-named-py4j-java-gateway
