import os
import sys

# Path for spark source folder
os.environ['SPARK_HOME'] = "C:\Apache\spark-1.4.1"

# Append pyspark to Python Path
sys.path.append("C:\Apache\spark-1.4.1\python")

from pyspark import SparkContext
from pyspark.sql import SQLContext

sc = SparkContext('local')
sqlContext = SQLContext(sc)
words = sc.parallelize(["scala","java","hadoop","spark","akka"])
print words.count()

path = os.path.join(os.environ['SPARK_HOME'], "examples/src/main/resources/people.json")

# Create the DataFrame
df = sqlContext.read.json(path)
df.show()

# Register this DataFrame as a table.
df.registerTempTable("people")

# SQL statements can be run by using the sql methods provided by sqlContext
teenagers = sqlContext.sql("SELECT name FROM people WHERE age >= 13 AND age <= 19")

teenagers.show()

for each in teenagers.collect():
    print(each[0])

sc.stop()
