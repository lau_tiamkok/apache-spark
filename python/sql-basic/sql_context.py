import os
import sys

# Path for spark source folder
os.environ['SPARK_HOME'] = "C:\Apache\spark-1.4.1"

# Append pyspark to Python Path
sys.path.append("C:\Apache\spark-1.4.1\python")

from pyspark import SparkContext
from pyspark import SparkConf
from pyspark.sql import SQLContext

import pandas as pd
import numpy as np

# This is our application object. It could have any name,
# except when using mod_wsgi where it must be "application"
def application(environ, start_response):

    sc = SparkContext(appName="PythonSQL")
    sqlContext = SQLContext(sc)

    path = os.path.join(os.environ['SPARK_HOME'], "examples/src/main/resources/people.json")

    # Create the DataFrame
    df = sqlContext.read.json(path)

    # Register this DataFrame as a table.
    df.registerTempTable("people")

    # SQL statements can be run by using the sql methods provided by sqlContext
    teenagers = sqlContext.sql("SELECT name FROM people")

    # Convert Spark DataFrame to Pandas
    pandas_df = df.toPandas()

    # Set an empty var.
    names = ""

    for each in teenagers.collect():
        names += each[0] + " "
        print(each[0])

    sc.stop()

    # Pandas dataframe to dictionary.
    dict_persons = pandas_df.to_dict()
    #print dict_persons

    # Set an empty var.
    persons = []
    str_persons=""

    # Loop the pandas dataframe.
    for index, row in pandas_df.iterrows():

        persons.append(str(row['name']) + ": " + str(row['age']))
        #persons += str(row['name']) + ": " + str(row['age']) + ", "

        print row['name'], row['age']

    #print persons

    # Loop the dict.
    for key in dict_persons['name']:

        persons.append(str(dict_persons['name'][key]) + ": " + str(dict_persons['age'][key]))
        #print "Name:",py_dict['name'][key]
        #print "Age:", py_dict['age'][key]
        #print

    # Join the array/ list by ','.
    str_persons = ", ".join(persons)

    #response_body = "Successfully imported Spark Modules and the names are: " + str(names)
    response_body = "Successfully imported Spark Modules and the teenagers are: " + str_persons

    # HTTP response code and message
    status = '200 OK'

    # These are HTTP headers expected by the client.
    # They must be wrapped as a list of tupled pairs:
    # [(Header name, Header value)].
    response_headers = [('Content-Type', 'text/plain'),
                       ('Content-Length', str(len(response_body)))]

    # Send them to the server using the supplied function
    start_response(status, response_headers)

    # Return the response body.
    # Notice it is wrapped in a list although it could be any iterable.
    return [response_body]
