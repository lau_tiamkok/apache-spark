#!/Python27/python
print "Content-type: text/html; charset=utf-8"
print

# enable debugging
import cgitb
cgitb.enable()

import os
import sys

# Path for spark source folder
os.environ['SPARK_HOME'] = "C:/Apache/spark-1.4.1"

# Append pyspark to Python Path
sys.path.append("C:/Apache/spark-1.4.1/python")

try:
    from pyspark import SparkContext
    from pyspark import SparkConf

    print ("Successfully imported Spark Modules")

except ImportError as e:
    print ("Can not import Spark Modules", e)
    sys.exit(1)
