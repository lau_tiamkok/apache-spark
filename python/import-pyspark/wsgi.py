import os
import sys

# Path for spark source folder
os.environ['SPARK_HOME'] = "C:/Apache/spark-1.4.1"

# Append pyspark to Python Path
sys.path.append("C:/Apache/spark-1.4.1/python")

# This is our application object. It could have any name,
# except when using mod_wsgi where it must be "application"
def application(environ, start_response):

    # build the response body possibly using the environ dictionary
    try:
        from pyspark import SparkContext
        from pyspark import SparkConf

        response_body = "Successfully imported Spark Modules"

    except ImportError as e:
        response_body = "Can not import Spark Modules" + e
        sys.exit(1)

    # HTTP response code and message
    status = '200 OK'

    # These are HTTP headers expected by the client.
    # They must be wrapped as a list of tupled pairs:
    # [(Header name, Header value)].
    response_headers = [('Content-Type', 'text/plain'),
                       ('Content-Length', str(len(response_body)))]

    # Send them to the server using the supplied function
    start_response(status, response_headers)

    # Return the response body.
    # Notice it is wrapped in a list although it could be any iterable.
    return [response_body]
