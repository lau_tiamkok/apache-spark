# Apache Spark + Python

1. Install Py4J - https://www.py4j.org/install.html

    Run `pip install py4j` or `easy_install py4j` in your CMD.

## Ref:

    * http://spark.apache.org/docs/latest/sql-programming-guide.html
    * https://github.com/apache/spark/blob/master/examples/src/main/python/sql.py
    * https://districtdatalabs.silvrback.com/getting-started-with-spark-in-python
    * https://districtdatalabs.silvrback.com/getting-started-with-spark-in-python
    * http://renien.github.io/blog/accessing-pyspark-pycharm/
    * http://stackoverflow.com/questions/26315726/importerror-no-module-named-py4j-java-gateway
    * https://www.py4j.org/install.html#install-instructions

# Apache Spark + Python + Pandas + Numpy

1. Install Numpy,

    `pip install C:\path-to-download-folder\numpy-1.10.0b1+mkl-cp27-none-win32.whl`

    Install a Python package with a .whl file,

    Ref:

    * http://stackoverflow.com/questions/27885397/how-do-i-install-a-python-package-with-a-whl-file

2. Install Pandas,

    `pip install pandas`

3. Convert Spark dataFrame to Pandas',

    ```
    # You can also incorporate SQL while working with DataFrames, using Spark SQL. This example counts the number of users in the young DataFrame.
    young.registerTempTable("young")
    context.sql("SELECT count(*) FROM young")

    # Convert Spark DataFrame to Pandas
    pandas_df = young.toPandas()

    # Create a Spark DataFrame from Pandas
    spark_df = context.createDataFrame(pandas_df)
    ```

## Ref:

    * http://www.gregreda.com/2013/10/26/intro-to-pandas-data-structures/
    * http://www.datarobot.com/blog/introduction-to-python-for-statistical-learning/
    * https://databricks.com/blog/2015/02/17/introducing-dataframes-in-spark-for-large-scale-data-science.html
    * http://stackoverflow.com/questions/32042634/python-loop-a-dictionary
    * http://stackoverflow.com/questions/32043042/python-join-how-to-join-a-data-in-loop
