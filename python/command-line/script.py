import os

from pyspark.sql import SQLContext

sc = SparkContext(appName="PythonSQL")
sqlContext = SQLContext(sc)

# Set the system environment variables.
# A JSON dataset is pointed to by path.
# The path can be either a single text file or a directory storing text files.
path = os.path.join(os.environ['SPARK_HOME'], "examples/src/main/resources/people.json")

# Create the DataFrame
df = sqlContext.read.json(path)
#df = sqlContext.read.json("C:\Apache/spark-1.4.1/bin/../examples/src/main/resources/people.json")

# Show the content of the DataFrame
df.show()

# Print the schema in a tree format
df.printSchema()

# Select only the "name" column
df.select("name").show()

# Select everybody, but increment the age by 1
df.select("name", df.age + 1).show()

# Register this DataFrame as a table.
df.registerTempTable("people")

# SQL statements can be run by using the sql methods provided by sqlContext
teenagers = sqlContext.sql("SELECT name FROM people WHERE age >= 13 AND age <= 19")

teenagers.show()

for each in teenagers.collect():
    print(each[0])

sc.stop()
