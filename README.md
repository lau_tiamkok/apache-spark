# Apache Spark: Standalone

If we don't want with Hadoop/EC2, we just want to run it in standalone mode on windows. Here we'll see how we can run Spark on Windows machine.

## Prerequisite

1. Make sure you have Java 8+ installed on your computer and the system environments set.

2. To test - open the Windows CMD, type

    `java -version`

3. Install Python 2.7.x - if you are working on Python with Spark.

## Download Spark

1. Download Spark from http://spark.apache.org/.

    You should follow the steps 1 to 3 to create a download link for a Spark Package of your choice. On the “2. Choose a package type” option, select any pre-built package type from the drop-down list (Figure 3). Since we want to experiment locally on windows, a pre-built package for Hadoop 2.6  and later will suffice.

    ref:

    * http://blog.danielemaasit.com/2015/07/26/installing-and-starting-sparkr-locally-on-windows-8-1-and-rstudio/

2. Unzip built package to c:/

3. Interact with Spark,

    `cd C:\Apache\Spark-1.4.1\bin`

    You use the commands `spark-shell.cmd` and `pyspark.cmd` to run Spark Shell using Scala and Python respectively.

    Option 2:

    Edit your your PATH and to set the SPARK_HOME environment variable.

    You should now be able to run a `pyspark` interpreter locally.

4. Spark Web Console,

    When Spark is running in any mode, you can view the Spark job results and other statistics by accessing Spark Web Console via the following URL:

    `http://localhost:4040`

## Ref:

    * http://www.infoq.com/articles/apache-spark-introduction
    * https://districtdatalabs.silvrback.com/getting-started-with-spark-in-python
    * http://nishutayaltech.blogspot.com/2015/04/how-to-run-apache-spark-on-windows7-in.html
