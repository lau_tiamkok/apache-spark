# Apache Spark + R: Installing and Starting SparkR Locally on Windows OS and RStudio

R is a popular statistical programming language for performing efficient data manipulation, visualization and machine learning tasks. However, interactive data analysis in R is usually limited as the runtime is single-threaded and can only process data sets that fit in a single machine’s memory. SparkR provides an R frontend to Apache Spark and using Spark’s distributed computation engine allows R-users to run large scale data analysis from the R shell. (ref: http://www.danielemaasit.com/getting-started-with-sparkr/)

Spark 1.4 introduces SparkR, an R API for Spark and Spark’s first new language API since PySpark was added in 2012. SparkR is based on Spark’s parallel DataFrame abstraction. Users can create SparkR DataFrames from “local” R data frames, or from any Spark data source such as Hive, HDFS, Parquet or JSON. SparkR DataFrames support all Spark DataFrame operations including aggregation, filtering, grouping, summary statistics, and other analytical functions. They also supports mixing-in SQL queries, and converting query results to and from DataFrames. Because SparkR uses the Spark’s parallel engine underneath, operations take advantage of multiple cores or multiple machines, and can scale to data sizes much larger than standalone R programs. (ref: https://databricks.com/blog/2015/06/11/announcing-apache-spark-1-4.html)

This announcement is great news if you'd like to use the flexibility of the R language to perform fast computations on very large data sets. Unlike map-reduce in Hadoop, Spark is a distributed framework specifically designed for interactive queries and iterative algorithms. The Spark DataFrame abstraction is a tabular data object similar to R's native data.frame, but stored in the cluster environment. This conceptual similarity lends itself to elegant processing from R, using a syntax similar to dplyr. The SparkR blog post provides this example, which should seem familiar to dplyr users.

This new R package will give R users access to many of the benefits of the Spark framework, including the ability to import data from many sources into Spark, where it can be analyzed using the optimized Spark DataFrame system. Spark computations are automatically distributed across all the cores and machines available on the Spark cluster, so this package can be used to analyze terabytes of data using R.

ref:

    * http://www.r-bloggers.com/sparkr-distributed-data-frames-with-spark-and-r/
    * https://databricks.com/blog/2015/01/09/spark-sql-data-sources-api-unified-data-access-for-the-spark-platform.html


1. Set System Environment

    Once you have opened RStudio, you need to set the system environment first. You have to point your R session to the installed version of SparkR. Replace the SPARK_HOME variable using the path to your Spark folder, such as “C:/Apache/Spark-1.4.1″,

    `Sys.setenv(SPARK_HOME = "C:/Apache/spark-1.4.1")`

2. Set the Library Paths

    `.libPaths(c(file.path(Sys.getenv("SPARK_HOME"), "R", "lib"), .libPaths()))`

3. Load SparkR Library

    You can now load SparkR just as you would any other R library using the library() command,

    `library(SparkR)`

4. Initialize Spark Context and SQL Context

    Initialize SparkR by creating a Spark context using the command sparkR.init(). The argument in this command is master = “local[N]”, where N stands for the number of threads that you want to use.

    Also, you need to create a SQL context to be able to work with DataFrames (the main abstraction in SparkR). Use the command sparkRSQL.init() to create a SQL context from your Spark context,

    ```
    sc <- sparkR.init(master = "local")
    sqlContext <- sparkRSQL.init(sc)
    ```

    ref:

    * http://blog.danielemaasit.com/2015/07/26/installing-and-starting-sparkr-locally-on-windows-8-1-and-rstudio/
    * http://www.r-bloggers.com/sparkr-with-rstudio-in-ubuntu-12-04/
